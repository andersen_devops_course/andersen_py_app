FROM python:3.7.11-alpine3.13
#RUN pip3 install markupsafe werkzeug jinja2 click itsdangerous Flask
RUN pip3 install Flask
WORKDIR /opt/app/
COPY ./app.py /opt/app/app.py

#CMD ["python3", "./app.py"]
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
