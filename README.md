# About #  
This is a repository with a simple Python web application on Flask which return "Hello world 1!" phrase.

## How to deploy ##

You can clone the repository to the environmet where you are going to run it.  
In order to prepare your environment you can refer to the GitLab repository [here](https://gitlab.com/andersen_devops_course/andersen_infra_deploy)  
The pipeline as a code is described in .gitlab-ci.yml file. There are 2 stages build and run.
* Under **build** stage we build an image and push it to GitLab registry
* During **run** stage we run docker-compose which pull new Docker image from GitLab Registry

Development of dev branch is still in progress...


