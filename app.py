from flask import Flask

app = Flask(__name__)
# set up route 
@app.route("/")
def home():
    return "Hello World 1! From Python app!\n"

if __name__ == "__main__":
    app.run()

